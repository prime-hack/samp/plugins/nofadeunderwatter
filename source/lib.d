import core.sys.windows.windows;
import core.sys.windows.dll;
import llmo.mem;
import std.conv : to;

extern (Windows) BOOL DllMain(HINSTANCE hinst,uint event,void*){
	final switch(event){
		case DLL_PROCESS_ATTACH:
			dll_process_attach(hinst);
			init;
			break;
		case DLL_PROCESS_DETACH:
			uninit;
			dll_process_detach(hinst);
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach();
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach();
			break;
	}
	return TRUE;
}

void init(){
	setMemory(0x0053DFEE, 0x90, 5, MemorySafe.safe);
}

void uninit(){
	writeMemory(0x0053DFEE, [0xE8, 0x2D, 0x52, 0x01, 0x00].to!(ubyte[]), MemorySafe.safe);
}
